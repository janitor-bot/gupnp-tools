Source: gupnp-tools
Section: net
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: @GNOME_TEAM@
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gnome,
               gettext (>= 0.19.7),
               libglib2.0-dev (>= 2.68),
               libgssdp-1.6-dev,
               libgupnp-1.6-dev,
               libgupnp-av-1.0-dev (>= 0.5.5),
               libsoup-3.0-dev,
               libxml2-dev (>= 2.0),
               libgtk-3-dev (>= 3.10),
               libgtksourceview-4-dev (>= 3.2.0),
               meson
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/gnome-team/gupnp-tools.git
Vcs-Browser: https://salsa.debian.org/gnome-team/gupnp-tools
Homepage: https://wiki.gnome.org/Projects/GUPnP

Package: gupnp-tools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: tools for testing UPnP devices and control points
 GUPnP is an object-oriented open source framework for creating UPnP devices
 and control points, written in C using GObject and libsoup. The GUPnP API is
 intended to be easy to use, efficient and flexible.
 .
 GUPnP Tools are free replacements of Intel UPnP tools that use GUPnP. They
 provides the following client and server side tools which enable one to easily
 test and debug one's UPnP devices and control points:
  * Universal Control Point: a tool that enables one to discover UPnP devices
    and services, retrieve information about them, subscribe to events and
    invoke actions.
  * Network Light: a virtual light bulb that allows control points to switch
    it on and off, change its dimming level and query its current status.
  * AV Control Point: a simple media player UI that enables one to discover and
    play multimedia content available on the network.
  * MediaServer upload: upload files to MediaServers
  * Upload: a simple commandline utility that uploads files to known
    MediaServers. Use Universal Control Point for discovering the MediaServers.
